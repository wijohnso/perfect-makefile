############################################################################## #
#@Filename : makefile
#@brief : builds objects Main
#optionally runs executable drivers
#optionally runs executable drivers with valgrind to test
#for memory leaks
#
#@copyright (C) 2024, Wil Johnson, All rights reserved
#
#This program is free software; you can redistribute it and / or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
#GNU General Public License for more details.
#
###############################################################################

# target name
TARGET := Main

# object (class) file names (without suffix)
OBJECTS := Blah

# maintain a directory structure as follows:
#	./bin		includes the target executable
#			(created and deleted by this makefile)
#
#	./include	includes all header (.h) files
#
#	./lib		includes all library (.a) files
#
#	./obj		includes build artifact object (.o) files
#			(created and deleted by this makefile)
#
#	./src		includes all c++ source (.cc) files
#
# recipes are as follows:
#	make (all)	creates the base executable with verbose warning
#			and error output
#
#	make gtest	creates the gtest-all and gtest_main static libraries
#
#	make clean	removes build artifact objects and binaries
#
#	make run	runs the target executable
#
#	make test	runs the target executable while checking for memory
#			leaks
#
# Code below this line is generic and should not be modified
###############################################################################
BIN := ./bin
INC := ./include
LIB := ./lib
OBJ := ./obj
REP := ./rep
RND := ./rand
SRC := ./src

TARG := $(addprefix $(BIN)/,$(TARGET))
OBJS := $(addsuffix .o, $(addprefix $(OBJ)/,$(OBJECTS))) $(addsuffix .o, $(addprefix $(OBJ)/,$(TARGET)))

GTEST := gtest-all gtest_main
LIBS := $(addsuffix .a, $(addprefix $(LIB)/,$(GTEST)))

CXX := clang++
CXX_FLAGS := -std=c++17 -pedantic -Wall -Wextra -Werror -Wshadow -Wsign-conversion -O3 -g
CLANG_FLAGS := -fprofile-instr-generate -fcoverage-mapping
CPP_FLAGS := -isystem $(INC)

all : clean mkdirs $(TARG)

debug: CXX_FLAGS += -DDEBUG_OUTPUT
debug: all

gtest : $(LIBS)

$(OBJ)/%.o : $(SRC)/$(SRC)/%.cc
	$(CXX) $(CPP_FLAGS) -I$(INC) -I$(SRC) $(CXX_FLAGS) -c $< -o $@

$(OBJ)/%.o : $(SRC)/%.cc
	$(CXX) $(CPP_FLAGS) $(CXX_FLAGS) -c $< -o $@

$(LIB)/%.a : $(OBJ)/%.o
	$(AR) $(ARFLAGS) $@ $^

$(TARG) : $(OBJS) $(LIBS)
	$(CXX) $(CPP_FLAGS) $(CXX_FLAGS) -pthread $^ -o $@

clean :
	#cleaning build artifacts
	@-rm -r $(BIN) $(OBJ) $(REP) $(RND) default.profraw 

mkdirs :
	#creating directory structure
	@-mkdir $(BIN) $(LIB) $(OBJ) $(RND)
	@-clear

coverage: CXX_FLAGS += $(CLANG_FLAGS)
coverage : clean mkdirs $(TARG)
	@-LLVM_PROFILE_FILE=$(REP)/report.raw $(TARG)
	@-llvm-profdata merge -sparse $(REP)/report.raw -o $(REP)/report
	@-llvm-cov show $(TARG) -instr-profile=$(REP)/report > $(REP)/report.txt -show-line-counts-or-regions
	@-llvm-cov report $(TARG) -instr-profile=$(REP)/report

run:
	$(TARG)
test :
	valgrind --tool=memcheck --leak-check=full --track-origins=yes -s $(TARG)

###############################################################################
# Optional directory setup
#
# Run once for project setup or teardown
#
#	make setup	sets up the basic file structure given the object
#			and target names specified above
#			adds #include directives where needed
#			adds main() function to driver
#			adds guard statements to includes
#			opens driver for editing in kate
#
#	make destroy	destroys the entire project except for this makefile
#
###############################################################################
DRIVER := $(addsuffix .cc, $(addprefix $(SRC)/,$(TARGET)))
DEFS := $(addsuffix .h, $(addprefix $(INC)/,Defs))

define Copyright
	echo "** @copyright (C) 2025, Wil Johnson, All rights reserved" >> $1/$2 ; \
	echo " **" >> $1/$2 ; \
	echo " ** This program is free software; you can redistribute it and/or" >> $1/$2 ; \
	echo " ** modify it under the terms of the GNU General Public License" >> $1/$2 ; \
	echo " ** as published by the Free Software Foundation; either version 2" >> $1/$2 ; \
	echo " ** of the License, or (at your option) any later version." >> $1/$2 ; \
	echo " **" >> $1/$2 ; \
	echo " ** This program is distributed in the hope that it will be useful," >> $1/$2 ; \
	echo " ** but WITHOUT ANY WARRANTY; without even the implied warranty of" >> $1/$2 ; \
	echo " ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" >> $1/$2 ; \
	echo " ** GNU General Public License for more details." >> $1/$2 ; \
	echo " **/" >> $1/$2 ; \
	echo "" >> $1/$2
endef

define File_Footer
	echo "" >> $1/$2 ; \
	echo -n "/" >> $1/$2 ; \
	$(call Copyright,$1,$2)
endef

define File_Header
	echo "/** @Filename: $2" >> $1/$2 ; \
	echo " ** @brief:    " >> $1/$2 ; \
	echo " **" >> $1/$2 ; \
	echo -n " " >> $1/$2 ; \
	$(call Copyright,$1,$2)
endef

define Function_Header
	echo "   /** $1()" >> $2/$3 ; \
	echo "    ** @brief:" >> $2/$3 ; \
	echo "    ** @params: none" >> $2/$3 ; \
	echo "    ** @return: none" >> $2/$3 ; \
	echo "    **/" >> $2/$3
endef

define Function_Header_Main
	echo "/** $1()" >> $2/$3 ; \
	echo " ** @brief:" >> $2/$3 ; \
	echo " ** @params: none" >> $2/$3 ; \
	echo " ** @return: none" >> $2/$3 ; \
	echo " **/" >> $2/$3
endef

setup:
	# setting up src files
	@-for FILE in $(OBJECTS) ; do \
		$(call File_Header,$(SRC),$$FILE.cc) ; \
		echo "#include \"$$FILE.h\"" >> $(SRC)/$$FILE.cc ; \
		echo "" >> $(SRC)/$$FILE.cc ; \
		echo "$$FILE::$$FILE() {" >> $(SRC)/$$FILE.cc ; \
		echo "" >> $(SRC)/$$FILE.cc ; \
		echo "} // $$FILE()" >> $(SRC)/$$FILE.cc ; \
		echo "" >> $(SRC)/$$FILE.cc ; \
		echo "$$FILE::~$$FILE() {" >> $(SRC)/$$FILE.cc ; \
		echo "" >> $(SRC)/$$FILE.cc ; \
		echo "} // ~$$FILE()" >> $(SRC)/$$FILE.cc ; \
		$(call File_Footer,$(SRC),$$FILE.cc) ; \
	done

	# setting up include files
	@-for FILE in $(OBJECTS) ; do \
		$(call File_Header,$(INC),$$FILE.h) ; \
		echo -n "#ifndef " >> $(INC)/$$FILE.h ; \
		echo $$FILE\_H | tr a-z A-Z >> $(INC)/$$FILE.h ; \
		echo -n "#define " >> $(INC)/$$FILE.h ; \
		echo $$FILE\_H | tr a-z A-Z >> $(INC)/$$FILE.h ; \
		echo "" >> $(INC)/$$FILE.h ; \
		echo "#include \"Defs.h\"" >> $(INC)/$$FILE.h ; \
		echo "" >> $(INC)/$$FILE.h ; \
		echo "class $$FILE {" >> $(INC)/$$FILE.h ; \
		echo "public:" >> $(INC)/$$FILE.h ; \
		$(call Function_Header,$$FILE,$(INC),$$FILE.h) ; \
		echo "  $$FILE();" >> $(INC)/$$FILE.h ; \
		echo "" >> $(INC)/$$FILE.h ; \
		$(call Function_Header,~$$FILE,$(INC),$$FILE.h) ; \
		echo "   ~$$FILE();" >> $(INC)/$$FILE.h ; \
		echo "" >> $(INC)/$$FILE.h ; \
		echo "protected:" >> $(INC)/$$FILE.h ; \
		echo "" >> $(INC)/$$FILE.h ; \
		echo "private:" >> $(INC)/$$FILE.h ; \
		echo "" >> $(INC)/$$FILE.h ; \
		echo "}; // class $$FILE" >> $(INC)/$$FILE.h ; \
		echo "" >> $(INC)/$$FILE.h ; \
		echo -n "#endif // " >> $(INC)/$$FILE.h ; \
		echo $$FILE\_H | tr a-z A-Z >> $(INC)/$$FILE.h ; \
		$(call File_Footer,$(INC),$$FILE.h) ; \
	done
	
	# setting up driver files
	@-$(call File_Header,$(SRC),$(TARGET).cc)
	@-echo "#include \"Defs.h\"" >> $(DRIVER)
	@-echo "" >> $(DRIVER)
	@-for FILE in $(OBJECTS) ; do \
		echo "#include \"$$FILE.h\"" >> $(DRIVER) ; \
	done
	@-echo "" >> $(DRIVER)
	@-echo "#include \"gtest/gtest.h\"" >> $(DRIVER)
	@-echo "" >> $(DRIVER)
	@-$(call Function_Header_Main,main,$(SRC),$(TARGET).cc)
	@-echo "TEST(TestGroup, Test){" >> $(DRIVER)
	@-echo "   EXPECT_EQ(10, 10);" >> $(DRIVER)
	@-echo "} // TEST(TestGroup, Test)" >> $(DRIVER)
	@-$(call File_Footer,$(SRC),$(TARGET).cc)

	# setting up defs file
	@-$(call File_Header,$(INC),Defs.h)
	@-echo -n "#include <stdint.h>" >> $(DEFS)
	@-echo "" >> $(DEFS)
	@-echo "" >> $(DEFS)
	@-echo -n "#ifndef " >> $(DEFS)
	@-echo DEFS_H | tr a-z A-Z >> $(DEFS)
	@-echo -n "#define " >> $(DEFS)
	@-echo DEFS_H | tr a-z A-Z >> $(DEFS)
	@-echo "" >> $(DEFS)
	@-echo "typedef uint8_t  U8;" >> $(DEFS)
	@-echo "typedef uint16_t U16;" >> $(DEFS)
	@-echo "typedef uint32_t U32;" >> $(DEFS)
	@-echo "typedef uint64_t U64;" >> $(DEFS)
	@-echo "" >> $(DEFS)
	@-echo -n "#endif // " >> $(DEFS)
	@-echo DEFS_H | tr a-z A-Z >> $(DEFS)
	@-$(call File_Footer,$(INC),Defs.h)

	# opening files in editor
	@-kate $(SRC)/*.cc $(INC)/*.h &

destroy: clean
	@-rm -r $(SRC)/*.cc $(INC)/*.h $(LIB)

###############################################################################
# @copyright (C) 2024, Wil Johnson, All rights reserved
#
# This program is free software; you can redistribute it and / or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU General Public License for more details.
#
###############################################################################
