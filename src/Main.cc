/** @Filename: Main.cc
 ** @brief:    
 **
 ** @copyright (C) 2025, Wil Johnson, All rights reserved
 **
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "Defs.h"

#include "Blah.h"

#include "gtest/gtest.h"

/** main()
 ** @brief:
 ** @params: none
 ** @return: none
 **/
TEST(TestGroup, Test){
   EXPECT_EQ(10, 10);
} // TEST(TestGroup, Test)

/** @copyright (C) 2025, Wil Johnson, All rights reserved
 **
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

